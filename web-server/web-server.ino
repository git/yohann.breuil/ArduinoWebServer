#include <Arduino.h>
#include <logging.h>
#include <uartWiFi.h>

#define LOG_LEVEL LOG_LEVEL_DEBUG

UARTWifi uartWifi;

bool isConnectedAP = false;

/**
 * Connect to AP
 * Parameters 
 *  ssid : char* - AP SSID
 *  password : char* - AP Password
 */
void connectAP(const char* ssid, const char* password) {
  if (!isConnectedAP) {
    isConnectedAP = uartWifi.connectAP(ssid, password);
  } 
}

void setup() {
  Serial.begin(9600);
  uartWifi.begin();
}

void loop() {
  LogInfo("Hello World");
  
  connectAP("Iphone Yohann", "12345678");

  WIFI_MODE wifiMode = uartWifi.getWiFiMode();

  delay(1000);
}
